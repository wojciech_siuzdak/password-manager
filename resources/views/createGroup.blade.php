<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CreateGroup</title>
    <link rel="stylesheet" href="../css/styles.css">
</head>
<body>
<div class="container-fluid">
    <div class="group-container hei">
        <form action="{{route('group.store')}}" method="POST">
            @csrf
            <label for="groupNAme">Podaj nazwę grupy</label>
            <input type="text" id="groupName" placeholder="Nazwa" name="name" required>
            </label>
            <br/>
            <label for="users">Wybierz których użytkowników dodać do grupy
                <br>
                <br>
                <select name="users[]" multiple required>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </label>
            <button type="submit">Dodaj</button>
        </form>
    </div>
    <div class="group-container hei">
        <p>Wybierz którą grupę chcesz zmodyfikować</p>
       <div class="mod2"> @foreach($group as $item)
               <div class="mod">
                   <a class="modifyGroupButton" href="{{route('group.edit',[$item])}}">{{$item->name}}</a>
                   <form class="form1" action="{{route('group.destroy', ['group'=>$item])}}" method="POST">
                       @csrf
                       @method('DELETE')
                       <button type="submit" class="usun1 usun">Usuń</button>
                   </form>
               </div>
           @endforeach</div>
    </div>
</div>
<form action="{{route('dashboard.index')}}" class="button-form logout-form" method="GET">
    <button type="submit">Wróć</button>
</form>
</body>
</html>
