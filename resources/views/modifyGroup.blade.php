<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ModifyGroup</title>
    <link rel="stylesheet" href="../../css/styles.css">
</head>
<body>
<div class="container-fluid">
    <div class="group-container modify-container">
        <form action="{{route('group.updateName',[$group])}}" method="POST">
            @csrf
            @method('PUT')
            <p>Podaj nazwę grupy</p>
            <input type="text" id="groupName" required name="name" value="{{$group->name}}">
            <button type="submit" class="pokaz modifybnt">Zmień</button>
        </form>
    </div>
    <div class="group-container modify-container">
        <form action="{{route('group.deleteUserFromGroup',[$group])}}" method="POST">
            @csrf
            @method('DELETE')
            <p> Wybierz którego użytkownika grupy usunąć</p>
            <select name="users[]" multiple required>
                @foreach($groupUsers as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                @endforeach
            </select>

            <button type="submit" class="usun modifybnt">Usun</button>
        </form>
    </div>
    <div class="group-container modify-container">
        <form action="{{route('group.addUserToGroup',[$group])}}" method="POST">
            @csrf
            @method('PUT')
            <p> Wybierz którego użytkownika dodać do grupy</p>
            <select name="users[]" multiple required>
                @foreach($usersWithoutGroup as $user)
                    <option value="{{$user['id']}}">{{$user['name']}}</option>
                @endforeach
            </select>
            <button type="submit" class="pokaz modifybnt">Dodaj</button>
        </form>
    </div>
</div>
<form action="{{route('group.index')}}" class="button-form logout-form" method="GET">
    <button type="submit">Wróć</button>
</form>
</body>
</html>
