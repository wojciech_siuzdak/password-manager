<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../css/styles.css">

</head>
<body>
<!DOCTYPE html>
<html>
<body>

<form action="{{route('dashboard.store')}}" class="form-add" method="POST">
    @csrf
    <input type="text" name="application_name" placeholder="nazwa aplikacji" required>
    <input type="input" name="password" placeholder="hasło" id="haslo" required>
    <button type="button" id="generuj">Generuj hasło</button>
    <select name="group_id[]" multiple >
        @foreach($groups as $group)
            <option value="{{$group->id}}">{{$group->name}}</option>
        @endforeach
    </select>
    <button type="submit">DODAJ</button>
</form>
{{--@role('admin')--}}
<form action="{{route('group.index')}}" class="button-form" method="GET">
    @csrf
    <button type="submit">Grupy</button>
</form>
{{--@role('admin')--}}
{{--<form  style = "margin-top: 10px"action="{{route('admin.index')}}" class="button-form" method="GET">--}}
{{--    <button type="submit">Panel Administratora</button>--}}
{{--</form>--}}
{{--@endrole--}}
<div class="container-app">
@foreach($passwords as $d)
    <div class="item-pass">
        <div class="info-item">
        <label for=""> Application name: </label>
        <input type="text" class="inp" disabled placeholder value="{{$d->application_name}}" >
        </div>
        <div class="info-item">
        <label for="{{$d->id}}">Password</label>
        <input type="password" class="inp" disabled placeholder value="{{$d->password}}" id="{{$d->id}}">
        </div>
        <button onclick="myFunction({{$d->id}})" class="pokaz">Pokaż</button>
        <form action="{{route('dashboard.destroy',$d)}}" style="align-self: flex-end;" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="usun">Usun</button>
        </form>
    </div>
@endforeach
</div>
<form action="{{route('logout')}}"  class="button-form logout-form" method="POST">
    @csrf
    <button type="submit">Wyloguj</button>
</form>

<script src="{{ mix('js/app.js') }}"></script>

<script>
    function myFunction(id) {
        var x = document.getElementById(id);
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
</script>
</body>
</html>

