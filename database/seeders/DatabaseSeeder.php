<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\Password;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'create group']);

        $role1 = Role::create(['name' => 'admin']);
        $role1->givePermissionTo('create group');


        /** @var User $user1 */
        $user1 = User::factory()->create([
            'name' => 'Wojciech Siuzdak',
            'email' => 'wojciech.siuzdak@evineo.net',
        ]);
        $user1->assignRole('admin');

        /** @var User $user2 */
        $user2 = User::factory()->create([
            'name' => 'Rafał Krawiec',
            'email' => 'rafal.krawiec@evineo.net',
        ]);

        /** @var Group $group */
        $group = Group::factory()->create();
        $user1->groups()->attach($group);
        $group->passwords()->attach(Password::factory()->create());
        $user1->passwords()->attach(Password::factory()->create());
        $user2->passwords()->attach(Password::factory()->create());

        Group::factory(5)->create();
    }
}
