<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Data
 *
 * @property int $id
 * @property string $application_name
 * @property string $password
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Password newModelQuery()
 * @method static Builder|Password newQuery()
 * @method static Builder|Password query()
 * @method static Builder|Password whereApplicationName($value)
 * @method static Builder|Password whereCreatedAt($value)
 * @method static Builder|Password whereId($value)
 * @method static Builder|Password wherePassword($value)
 * @method static Builder|Password whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Password extends Model
{
    use HasFactory;

    protected $fillable = [
        'application_name',
        'password',
    ];
}
