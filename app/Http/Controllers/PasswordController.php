<?php


namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Password;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class PasswordController extends Controller
{

    public function index(): Response
    {
        $user = Auth::user();
        $userId =  $user->id;

        $ids = DB::table('passwords')->distinct()->select('id')
            ->leftJoin('user_passwords', 'passwords.id', '=', 'user_passwords.password_id')
            ->leftJoin('group_passwords', 'passwords.id', '=', 'group_passwords.password_id')
            ->leftJoin('user_groups', 'group_passwords.group_id', '=', 'user_groups.group_id')
            ->where('user_passwords.user_id', '=', $userId)
            ->orWhere('user_groups.user_id', '=', $userId);

        $passwords = Password::query()->whereIn('id', $ids)->get();

        foreach ($passwords as $password){
            $password->password = Crypt::decryptString($password->password);
        }
        $groups = $user->groups()->get();

        return response()->view('dashboard', compact('passwords', 'groups'));
    }

    public function store(Request $request): RedirectResponse
    {
        $pass =  $request->input('password');
        $encryptPassword =  Crypt::encryptString($pass);
        $password = Password::make(['application_name'=>$request->input('application_name'),
            'password'=>$encryptPassword]);
        $password->save();

        if($request->input('group_id')) {
            $groups = Group::query()->whereIn('id', $request->input('group_id'))->get();

            foreach ($groups as $group) {
                $group->passwords()->attach($password);
            }
        }

        $user = User::findOrFail(Auth::id());
        $user->passwords()->attach($password);

        return redirect()->back();
    }


    public function destroy(Password $password): RedirectResponse
    {
        $password->delete();

        return redirect()->back();
    }
}
//Todo wysyłanie maila do odzyskania hasła

