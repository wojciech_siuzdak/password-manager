<?php


namespace App\Http\Controllers;


use App\Models\Group;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpFoundation\Response;

class GroupController extends Controller
{

//    public function adminIndex(): Response
//    {
//        $user = User::findOrFail(1)->hasAnyRole('admin');
//        $group = Group::all();
//        $users = User::all();
//
//        return response()->view('createGroup', compact('group', 'users'));
//    }

    public function index(): Response
    {
        $user = Auth::user();
        $users = User::all();

        if ($user->hasAnyRole('admin')) {
            $group = Group::all();
        } else {
            $group = $user->groups()->get();
        }

        return response()->view('createGroup', compact('group', 'users'));
    }

    public function store(Request $request): Response
    {
        $group = Group::create($request->all());

        foreach ($request->input('users') as $user) {
            User::findOrFail($user)->groups()->attach($group);
        }

        return redirect('dashboard');
    }

    public function edit(Group $group): Response
    {
        $groupUsers = $group->users()->get();
        $users = User::all();

        $usersWithoutGroup = array_udiff($users->toArray(),$groupUsers->toArray(),
            function ($obj_a, $obj_b) {
                if ($obj_a['id'] !== $obj_b['id']) {
                    return -1;
                } else {
                    return 0;
            }
            });

        return response()->view('modifyGroup', compact('groupUsers', 'group', 'usersWithoutGroup'));
    }

    public function updateName(Group $group, Request $request): Response
    {
        $group->update($request->all());

        return redirect()->route('group.edit', [$group]);
    }

    public function deleteUserFromGroup(Group $group, Request $request): Response
    {

        $users = User::whereIn('id', $request->input('users'))->get();

        foreach ($users as $user) {
            $user->groups()->detach($group);
        }
        return redirect()->route('group.edit', [$group]);
    }

    public function addUserToGroup(Group $group, Request $request): Response
    {
        $users = User::findOrFail($request->input('users'));

        foreach ($users as $user) {
            $user->groups()->attach($group);
        }
        return redirect()->route('group.edit', [$group]);
    }

    public function destroy(Group $group): RedirectResponse
    {
       // dd($group);
        $group->delete();

        return redirect()->route('group.index');

//        return Redirect::to('group');
    }
}
