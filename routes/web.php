<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\PasswordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::middleware(['auth'])->group(function (){


    Route::get('dashboard', [PasswordController::class, 'index'])->name('dashboard.index');
    Route::post('dashboard', [PasswordController::class, 'store'])->name('dashboard.store');
    Route::delete('dashboard/{password}', [PasswordController::class, 'destroy'])->name('dashboard.destroy');

    Route::get('group',[GroupController::class,'index'])->name('group.index');
    Route::post('group', [GroupController::class, 'store'])->name('group.store');
    Route::delete('group/{group}', [GroupController::class, 'destroy'])->name('group.destroy');
    Route::get('group/{group}/edit', [GroupController::class, 'edit'])->name('group.edit');
    Route::put('group/{group}/updateName', [GroupController::class, 'updateName'])->name('group.updateName');
    Route::delete('group/{group}/deleteUserFromGroup', [GroupController::class, 'deleteUserFromGroup'])->name('group.deleteUserFromGroup');
    Route::put('group/{group}/addUserToGroup', [GroupController::class, 'addUserToGroup'])->name('group.addUserToGroup');

    Route::get('admin',[AdminController::class,'index'])->name('admin.index');
});

require __DIR__ . '/auth.php';

